import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:like_button/like_button.dart';

class RepliesPage extends StatefulWidget {
  String Comment;
  RepliesPage({this.Comment});
  @override
  _RepliesPageState createState() => _RepliesPageState(Comments: Comment);
}

class _RepliesPageState extends State<RepliesPage> {
  String Comments;
  _RepliesPageState({this.Comments});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomAppBar(
        child: Row(
          children: [
            Container(
              padding: EdgeInsets.only(left: 10),
              margin: EdgeInsets.only( right: 10),
              decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey[300],
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(20))
              ),
              alignment: Alignment.centerLeft,
              width: MediaQuery.of(context).size.width * 2.4/3,
              height: MediaQuery.of(context).size.height/20,
              child :TextField(
                decoration: InputDecoration(
                    border: InputBorder.none,

                    hintText: 'Write a reply'
                ),
              ),
            ),
            IconButton(
              onPressed: (){},
              icon: Icon(Icons.send),
            )
          ],
        ) ,
      ),
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black87),
        backgroundColor: Colors.white,
        title: Text("Replies",
        style: TextStyle(
          color: Colors.black87
        ),),
      ),
      body: ListView(
        children: [
          MainComment(Comment: Comments,),
          Reply(
            ProfileImage: "Images/elephant.jpg",
            text: "We have to deal with this however we can, but it "
                "Becoming really difficult",
          ),
          Reply(
              ProfileImage: "Images/rhino.jpg",
              text: "We have to deal with this however we can, but it "
                  "Becoming really difficult"
          ),
          Reply(ProfileImage: "Images/rhino.jpg",
              text: "We have to deal with this however we can, but it "
                  "Becoming really difficult"),
          Reply(ProfileImage: "Images/rhino.jpg",
              text: "We have to deal with this however we can, but it "
                  "Becoming really difficult"),
          Reply(ProfileImage: "Images/dog.jpg",
              text: "We have to deal with this however we can, but it "
                  "Becoming really difficult"),
        ],
      ),
    );
  }
}
 class MainComment extends StatelessWidget {
  String Comment;
  MainComment({this.Comment});
   @override
   Widget build(BuildContext context) {
     return Container(
         margin: EdgeInsets.all(10),
         child: Column(
           children: [
             Container(
               alignment: Alignment.centerLeft,
               margin: EdgeInsets.all(20),
               child:  CircleAvatar(
                 child: Image.asset("Images/rhino.jpg"),
               ),
             ),
             Container(
               padding: EdgeInsets.only(top: 10,right: 10,bottom: 10,left: 10),
               child: Container(
                 child: Text(Comment,
                   style: TextStyle(
                     fontSize: 18,
                   ),),
               ),
             ),
             Container(
               padding: EdgeInsets.all(10),
               child: LikeButton(
                 mainAxisAlignment: MainAxisAlignment.start,
                 likeCount: 0,
                 size: 25,),
             ),
           ],
         ),
     );
   }
 }
class Reply extends StatelessWidget {
  String ProfileImage;
  String text;
  Reply({this.ProfileImage,this.text});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: MediaQuery.of(context).size.width -
          MediaQuery.of(context).size.width * 2.84/3, right: 10),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.all(10),
                child:  CircleAvatar(
                  child: Image.asset(
                      ProfileImage
                  ),
                ),
              ),
              Stack(
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 10,right: 10,bottom: 10),
                    child: Container(
                      width: MediaQuery.of(context).size.width * 2.1733/3,
                      color: Colors.grey[300],
                      child: Text(text,
                        style: TextStyle(
                          fontSize: 18,
                        ),),
                    ),
                  ),
                  Positioned(
                    bottom: 4.5,
                    right: 0,
                    child: Container(
                      color: Colors.white,
                      child: LikeButton(
                        likeCount: 0,
                        size: 15,

                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
          IconButton(
            icon: Icon(Icons.reply),
            onPressed: (){},
          )
        ],
      )
    );
  }
}