import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:like_button/like_button.dart';
import 'package:talk_it_out/RepliesPage.dart';
import 'package:sticky_headers/sticky_headers.dart';

class CommentPage extends StatelessWidget {
  String ProfileImage;
  String text;
  CommentPage({this.ProfileImage,this.text});
  RaisedButton Top = new RaisedButton(onPressed: (){},
  child: Text("Top"),);
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 1,
      child: MaterialApp(
        home: Scaffold(
          body: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                expandedHeight: 200,
                pinned: true,
                backgroundColor: Colors.white,
                floating: false,
                actionsIconTheme: IconThemeData(color: Colors.black87),
                actions: [
                  IconButton(
                    onPressed: (){},
                    icon: Icon(Icons.notifications_none),
                  ),
                ],
                flexibleSpace: FlexibleSpaceBar(
                  background: Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                ProfileImage,
                              ),
                              fit: BoxFit.cover
                          )
                      ),
                      child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width * 2.5/3,
                                alignment: Alignment.center,
                                padding: EdgeInsets.all(20),
                                child: Text(text,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.white
                                  ),
                                ),
                              ),
                              LikeButton(
                                likeCount: 0,
                              ),
                            ],
                          )
                      )
                  ),
                ),
              ),

              SliverToBoxAdapter(
                child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.pink,
                              ),
                              height: 30,
                              width: 50,
                              child: Center(
                                  child: Text("Top",
                                  style: TextStyle(
                                    color: Colors.white
                                  ),)
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(color: Colors.pink),
                                color: Colors.white,
                              ),
                              height: 30,
                              width: 50,
                              child: RaisedButton(
                                color: Colors.white,
                                onPressed: (){
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => NewNavigate(
                                      ProfileImage: ProfileImage,
                                      text: text,
                                    )),
                                  );

                                },
                                child: Center(
                                    child: Text("New",
                                      style: TextStyle(
                                          fontSize: 8,
                                          color: Colors.pink
                                      ),)
                                ),
                              )
                            ),
                            Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.pink),
                                  color: Colors.white,
                                ),
                                height: 30,
                                child: RaisedButton(
                                  color: Colors.white,
                                  onPressed: (){
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => CounsellarNavigate(
                                        ProfileImage: ProfileImage,
                                        text: text,
                                      )),
                                    );
                                  },
                                  child: Center(
                                      child: Text("Counsellar",
                                        style: TextStyle(
                                            fontSize: 8,
                                            color: Colors.pink
                                        ),)
                                  ),
                                )
                            ),
                            Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(color: Colors.pink),
                                  color: Colors.white,
                                ),
                                height: 30,
                                child: RaisedButton(
                                  color: Colors.white,
                                  onPressed: (){
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => PersonalNavigate(
                                        ProfileImage: ProfileImage,
                                        text: text,
                                      )),
                                    );
                                  },
                                  child: Center(
                                      child: Text("Personal",
                                        style: TextStyle(
                                            fontSize: 8,
                                            color: Colors.pink
                                        ),)
                                  ),
                                )
                            )
                          ],
                        ),
                        Comments(
                            Comment: "Anxiety is one of the most common  menatl diesease and the least"
                                "talked about one. It constantly harms our mental piece",
                            ProfileImage: "Images/dog.jpg"
                        ),
                        Reply(
                          ProfileImage: "Images/elephant.jpg",
                          text: "We have to deal with this however we can, but it "
                              "Becoming really difficult",
                        ),
                        Reply(
                            ProfileImage: "Images/rhino.jpg",
                            text: "We have to deal with this however we can, but it "
                                "Becoming really difficult"
                        ),
                        InsertReply(),
                        ViewMoreReply(
                            Comment: "Anxiety is one of the most common  menatl diesease and the least"
                                "talked about one. It constantly harms our mental piece"
                        ),
                        Comments(
                          Comment: "These days due to the amount of time I spend on screen"
                              ", I am disinterested in watching TV shows  and movies",
                          ProfileImage: "Images/elephant.jpg",
                        ),
                        Reply(ProfileImage: "Images/rhino.jpg",
                            text: "We have to deal with this however we can, but it "
                                "Becoming really difficult"),
                        Reply(ProfileImage: "Images/rhino.jpg",
                            text: "We have to deal with this however we can, but it "
                                "Becoming really difficult"),
                        Reply(ProfileImage: "Images/dog.jpg",
                            text: "We have to deal with this however we can, but it "
                                "Becoming really difficult"),
                        InsertReply(),
                        ViewMoreReply(
                            Comment: "Anxiety is one of the most common  menatl diesease and the least"
                                "talked about one. It constantly harms our mental piece"
                        )
                      ],
                    )

                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}



class InsertReply extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10),
      margin: EdgeInsets.only(left: MediaQuery.of(context).size.width -
          MediaQuery.of(context).size.width * 2.4/3, right: 10),
      decoration: BoxDecoration(
          border: Border.all(
            color: Colors.grey[300],
          ),
          borderRadius: BorderRadius.all(Radius.circular(20))
      ),
      alignment: Alignment.centerLeft,
      width: MediaQuery.of(context).size.width * 2.4/3,
      height: MediaQuery.of(context).size.height/20,
      child :TextField(
        decoration: InputDecoration(
            border: InputBorder.none,

            hintText: 'Write a reply'
        ),
      ),
    );
  }
}


class Reply extends StatelessWidget {
  String ProfileImage;
  String text;
  Reply({this.ProfileImage,this.text});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: MediaQuery.of(context).size.width -
          MediaQuery.of(context).size.width * 2.84/3, right: 10),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.all(10),
            child:  CircleAvatar(
              child: Image.asset(
                ProfileImage
              ),
            ),
          ),
          Stack(
            children: [
              Container(
                padding: EdgeInsets.only(top: 10,right: 10,bottom: 10),
                child: Container(
                  width: MediaQuery.of(context).size.width * 2.1733/3,
                  color: Colors.grey[300],
                  child: Text(text,
                    style: TextStyle(
                      fontSize: 18,
                    ),),
                ),
              ),
              Positioned(
                bottom: 4.5,
                right: 0,
                child: Container(
                  color: Colors.white,
                  child: LikeButton(
                    likeCount: 0,
                    size: 15,

                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class ViewMoreReply extends StatelessWidget {
  String Comment ;
  ViewMoreReply({this.Comment});
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: Colors.white,
      elevation: 0,
      child: Center(
        child : Text(
          "View all Replies",
          style: TextStyle(
              color: Colors.pink
          ),
        ),
      ),
      onPressed: (){
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => RepliesPage(
            Comment : Comment,
          )),
        );
      },
    );
  }
}

class Comments extends StatelessWidget {
  String ImageURL;
  String ProfileImage;
  String Comment;
  String text;
  Comments({this.Comment, this.ProfileImage, this.ImageURL,this.text});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              alignment: Alignment.topCenter,
              margin: EdgeInsets.only(left: 10,right: 10,bottom: 10),
              child:  CircleAvatar(
                backgroundColor: Colors.red,
                child: Image.asset(ProfileImage,fit: BoxFit.cover,),
              ),
            ),
            Stack(
              children: [
                Container(
                  padding: EdgeInsets.only(top: 10,right: 10,bottom: 10),
                  child: Container(
                    width: MediaQuery.of(context).size.width * 2.4/3,
                    color: Colors.grey[300],
                    child: Text(Comment,
                      style: TextStyle(
                        fontSize: 18,
                      ),),
                  ),
                ),
                Positioned(
                  bottom: 4.5,
                  right: 0,
                  child: Container(
                    color: Colors.white,
                    child: LikeButton(
                      likeCount: 0,
                      size: 15,

                    ),
                  ),
                ),
              ],
            )
          ],
        ),

      ],
    );
  }
}

class NewNavigate extends StatelessWidget {
  String ProfileImage;
  String text;
  NewNavigate({this.ProfileImage, this.text});
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 1,
      child: MaterialApp(
        home: Scaffold(
          body: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                expandedHeight: 200,
                pinned: true,
                backgroundColor: Colors.white,
                floating: false,
                actionsIconTheme: IconThemeData(color: Colors.black87),
                actions: [
                  IconButton(
                    onPressed: (){},
                    icon: Icon(Icons.notifications_none),
                  ),
                ],
                flexibleSpace: FlexibleSpaceBar(
                  background: Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                ProfileImage,
                              ),
                              fit: BoxFit.cover
                          )
                      ),
                      child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width * 2.5/3,
                                alignment: Alignment.center,
                                padding: EdgeInsets.all(20),
                                child: Text(text,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.white
                                  ),
                                ),
                              ),
                              LikeButton(
                                likeCount: 0,
                              ),
                            ],
                          )
                      )
                  ),
                ),
              ),


            ],
          ),
        ),
      ),
    );
  }
}

class CounsellarNavigate extends StatelessWidget {
  // ignore: non_constant_identifier_names
  String ProfileImage;
  String text;
  CounsellarNavigate({this.ProfileImage, this.text});
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 1,
      child: MaterialApp(
        home: Scaffold(
          body: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                expandedHeight: 200,
                pinned: true,
                backgroundColor: Colors.white,
                floating: false,
                actionsIconTheme: IconThemeData(color: Colors.black87),
                actions: [
                  IconButton(
                    onPressed: (){},
                    icon: Icon(Icons.notifications_none),
                  ),
                ],
                flexibleSpace: FlexibleSpaceBar(
                  background: Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                ProfileImage,
                              ),
                              fit: BoxFit.cover
                          )
                      ),
                      child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width * 2.5/3,
                                alignment: Alignment.center,
                                padding: EdgeInsets.all(20),
                                child: Text(text,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.white
                                  ),
                                ),
                              ),
                              LikeButton(
                                likeCount: 0,
                              ),
                            ],
                          )
                      )
                  ),
                ),
              ),


            ],
          ),
        ),
      ),
    );
  }
}

class PersonalNavigate extends StatelessWidget {
  String ProfileImage;
  String text;
  PersonalNavigate({this.ProfileImage, this.text});
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 1,
      child: MaterialApp(
        home: Scaffold(
          body: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                expandedHeight: 200,
                pinned: true,
                backgroundColor: Colors.white,
                floating: false,
                actionsIconTheme: IconThemeData(color: Colors.black87),
                actions: [
                  IconButton(
                    onPressed: (){},
                    icon: Icon(Icons.notifications_none),
                  ),
                ],
                flexibleSpace: FlexibleSpaceBar(
                  background: Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                ProfileImage,
                              ),
                              fit: BoxFit.cover
                          )
                      ),
                      child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width * 2.5/3,
                                alignment: Alignment.center,
                                padding: EdgeInsets.all(20),
                                child: Text(text,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.white
                                  ),
                                ),
                              ),
                              LikeButton(
                                likeCount: 0,
                              ),
                            ],
                          )
                      )
                  ),
                ),
              ),


            ],
          ),
        ),
      ),
    );
  }
}