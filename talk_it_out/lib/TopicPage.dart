import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' ;
import 'package:like_button/like_button.dart';
import 'package:talk_it_out/CommentPage.dart';

class TopicsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return Scaffold(
      appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            actionsIconTheme: IconThemeData(
              color: Colors.black,
            ),
            leading: IconButton(
              icon: Icon(Icons.menu),
              onPressed: (){},
              color: Colors.black,
            ),
            actions: [
              IconButton(
                  icon: Icon(Icons.info_outline),
                  onPressed: (){}
              ),
              IconButton(
                  icon: Icon(Icons.notifications_none),
                  onPressed: (){}
                  )
            ],
            title: Text(
                "TalkItOut",
                 style: TextStyle(
                   color: Colors.black,
                 ),),
      ),
      body: ListView(
        children: [
          Topic(
            ImageURL: "Images/anger.jpg",
            ImageText: "Is Anger the most common way to vent out stress?",
          ),
          CommentBox(
            Comment: "Thanks for putting this out there. I have been really "
                "worried in recent times as I have been getting angry on random"
                "Ocassions",
            ProfileImage: "Images/bear.jpg" ,
            ImageURL: "Images/anger.jpg",
            text: "Is Anger the most common way to vent out stress?",
          ),
          ViewMore(
              ImageURL: "Images/anger.jpg",
              text: "Is Anger the most common way to vent out stress?"
          ),
          Topic(
            ImageURL: "Images/anxiety.jpg",
            ImageText: "Is Anxiety causing trouble in our life?",
          ),
          CommentBox(
            Comment: "Anxiety is one of the most common  menatl diesease and the least"
                "talked about one. It constantly harms our mental piece",
            ProfileImage: "Images/dog.jpg",
            ImageURL: "Images/anxiety.jpg",
            text: "Is Anxiety causing trouble in our life?",
          ),
          ViewMore(
            ImageURL: "Images/anxiety.jpg",
            text: "Is Anxiety causing trouble in our life?",
          ),
          Topic(
            ImageURL: "Images/reading.jpg",
            ImageText: "Is reading going to remerge to reduce our screentime? ",
          ),
          CommentBox(
            Comment: "These days due to the amount of time I spend on screen"
                ", I am disinterested in watching TV shows  and movies",
            ProfileImage: "Images/elephant.jpg",
            ImageURL: "Images/reading.jpg",
            text: "Is reading going to remerge to reduce our screentime? ",
          ),
          ViewMore(
            ImageURL: "Images/reading.jpg",
            text: "Is reading going to remerge to reduce our screentime? ",
          ),
          Topic(
            ImageURL: "Images/eating.jpg",
            ImageText: "Is your diet in this pandemic health enough",
          ),
          CommentBox(
            Comment: "COVID has had a major impact on my diet. Even though I am not"
                " eating outside food , I am consuming a lot of junk food",
            ProfileImage: "Images/rhino.jpg",
            ImageURL: "Images/eating.jpg",
            text: "Is your diet in this pandemic health enough",
          ),
          ViewMore(
            ImageURL: "Images/eating.jpg",
            text: "Is your diet in this pandemic health enough",
          ),

        ],
      )

    );
  }
}

class Topic extends StatelessWidget {
  String ImageURL;
  String ImageText;
  Topic({this.ImageURL,this.ImageText});
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.fromLTRB(0, 0, 0, 5),
      child: Column(
        children: [
          AspectRatio(
            aspectRatio: 16/9,
            child: Container(
              child: Center(
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Text(ImageText,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                      ),),
                  )
              ),
              decoration: BoxDecoration(
                image: DecorationImage(
                    colorFilter:
                    ColorFilter.mode(Colors.black87.withOpacity(0.5),
                        BlendMode.hue),
                    image: AssetImage(
                    ImageURL,
                    ),
                    fit: BoxFit.cover
                ),

              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              LikeButton(
                likeCount: 0,
              ),
              IconButton(
                icon: Icon(Icons.comment),
                color: Colors.black87,
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CommentPage(
                      ProfileImage: ImageURL,
                      text: ImageText,
                    )),
                  );
                },
              )
            ],
          ),

        ],
      ),
    );
  }
}

class CommentBox extends StatelessWidget {
  String ImageURL;
  String ProfileImage;
  String Comment;
  String text;
  CommentBox({this.Comment, this.ProfileImage, this.ImageURL,this.text});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              alignment: Alignment.topCenter,
              margin: EdgeInsets.only(left: 10,right: 10,bottom: 10),
              child:  CircleAvatar(
                backgroundColor: Colors.red,
                child: Image.asset(ProfileImage,fit: BoxFit.cover,),
              ),
            ),
            Stack(
              children: [
                RaisedButton(
                  onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => CommentPage(
                        ProfileImage: ImageURL,
                        text: text,
                      )),
                    );
                  },
                  padding: EdgeInsets.only(top: 10,right: 10,bottom: 10),
                  child: Container(
                    width: MediaQuery.of(context).size.width * 2.4/3,
                    color: Colors.grey[300],
                    child: Text(Comment,
                      style: TextStyle(
                        fontSize: 18,
                      ),),
                  ),
                ),
                Positioned(
                  bottom: 4.5,
                  right: 0,
                  child: Container(
                    color: Colors.white,
                    child: LikeButton(
                      likeCount: 0,
                      size: 15,

                    ),
                  ),
                ),
              ],
            )
          ],
        ),

      ],
    );
  }
}

class ViewMore extends StatelessWidget {
  String ImageURL;
  String text;
  ViewMore({this.ImageURL,this.text});
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: Colors.white,
      elevation: 0,
      child: Center(
        child : Text(
          "View all 15 Comments",
          style: TextStyle(
              color: Colors.pink
          ),
        ),
      ),
      onPressed: (){
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => CommentPage(
            ProfileImage: ImageURL,
            text: text,
          )),
        );
      },
    );
  }
}
